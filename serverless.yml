service: klugger-back


plugins:
  - serverless-dynamodb-local
  - serverless-offline

custom:
  usersTableName: 'user-prod'
  likesTableName: 'likes-prod'
  addressTableName: 'address-prod'
  propertiesTableName: 'property-prod'
  landingEmailsTableName: 'landing-prod'
  propertiesImages: 'propertiesklugger'
  usersImages: 'usersklugger'
  dynamodb:
    start:
      migrate: true

provider:
  name: aws
  runtime: nodejs8.10
  stage: prod
  region: us-east-1
  iamRoleStatements:
    - Effect: Allow
      Action:
        - "s3:*"
      Resource:
        - "arn:aws:s3:::${self:custom.propertiesImages}/*"
        - "arn:aws:s3:::${self:custom.usersImages}/*"
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource:
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.propertiesTableName}
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.propertiesTableName}/index/userId-index
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.propertiesTableName}/index/operation-index
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.likesTableName}
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.likesTableName}/index/propertyId-index
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.usersTableName}
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.usersTableName}/index/id-index
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.landingEmailsTableName}
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.addressTableName}
        - arn:aws:dynamodb:us-east-1:693765878155:table/${self:custom.addressTableName}/index/stateId-municipalityId-index
  environment:
    USERS_TABLE: ${self:custom.usersTableName}
    ADDRESSES_TABLE: ${self:custom.addressTableName}
    LIKES_TABLE: ${self:custom.likesTableName}
    LANDING_TABLE : ${self:custom.landingEmailsTableName}
    PROPERTIES_TABLE: ${self:custom.propertiesTableName}
    USERS_IMAGES: ${self:custom.usersImages}   #users images
    PROPERTIES_IMAGES: ${self:custom.propertiesImages} #properties images
    URL_PROPERTIES_IMAGES: http://${self:custom.propertiesImages}.s3.amazonaws.com
    URL_USERS_IMAGES: http://${self:custom.usersImages}.s3.amazonaws.com
    MAX_SIZE_IMAGE: "10485760" #10*1024*1024
    EXPIRE_TOKEN: "2d" #60*60*24

functions:
  properties:
    handler: src/properties.handler
    timeout: 30
    events:
      - http: 'ANY /properties/{proxy+}'
        cors: true
  users:
    handler: src/users.handler
    timeout: 30
    events:
      - http: 'ANY /users/{proxy+}'
        cors: true
  admin:
    handler: src/admin.handler
    timeout: 30
    events:
      - http: 'ANY /admin/{proxy+}'
        cors: true
  addresses:
    handler: src/addresses.handler
    timeout: 30
    events:
      - http: 'ANY /addresses/{proxy+}'
        cors: true
resources:
  Resources:
    UsersImages:
      Type: AWS::S3::Bucket
      Properties:
        BucketName: ${self:custom.usersImages}
        AccessControl: PublicRead
        CorsConfiguration:
          CorsRules:
          - AllowedMethods:
            - GET
            - PUT
            - POST
            - HEAD
            AllowedOrigins:
            - "*"
            AllowedHeaders:
            - "*"
    PropertiesImages:
      Type: AWS::S3::Bucket
      Properties:
        BucketName: ${self:custom.propertiesImages}
        AccessControl: PublicRead
        CorsConfiguration:
          CorsRules:
          - AllowedMethods:
            - GET
            - PUT
            - POST
            - HEAD
            AllowedOrigins:
            - "*"
            AllowedHeaders:
            - "*"
    UsersTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        AttributeDefinitions:
          - AttributeName: providerId
            AttributeType: S
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: providerId
            KeyType: HASH
          - AttributeName: id
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 5
          WriteCapacityUnits: 5
        TableName: ${self:custom.usersTableName}
        GlobalSecondaryIndexes:
          - IndexName: id-index
            KeySchema:
              - AttributeName: id
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: 5
              WriteCapacityUnits: 5
    LandingEmailsTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        AttributeDefinitions:
          - AttributeName: userId
            AttributeType: S
          - AttributeName: email
            AttributeType: S
        KeySchema:
          - AttributeName: email
            KeyType: HASH
          - AttributeName: userId
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 5
          WriteCapacityUnits: 5
        TableName: ${self:custom.landingEmailsTableName}
    PropertiesTableDynamoDBTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        AttributeDefinitions:
          - AttributeName: propertyId
            AttributeType: S
          - AttributeName: operation
            AttributeType: S
          - AttributeName: userId
            AttributeType: S
        KeySchema:
          - AttributeName: propertyId
            KeyType: HASH
          - AttributeName: operation
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 5
          WriteCapacityUnits: 5
        TableName: ${self:custom.propertiesTableName}
        GlobalSecondaryIndexes:
          - IndexName: operation-index
            KeySchema:
              - AttributeName: operation
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: 5
              WriteCapacityUnits: 5
          - IndexName: userId-index
            KeySchema:
              - AttributeName: userId
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: 5
              WriteCapacityUnits: 5
    LikesTableDynamoDBTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        AttributeDefinitions:
          - AttributeName: userId
            AttributeType: S
          - AttributeName: propertyId
            AttributeType: S
        KeySchema:
          - AttributeName: userId
            KeyType: HASH
          - AttributeName: propertyId
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 5
          WriteCapacityUnits: 5
        TableName: ${self:custom.likesTableName}
        GlobalSecondaryIndexes:
          - IndexName: propertyId-index
            KeySchema:
              - AttributeName: propertyId
                KeyType: HASH
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: 5
              WriteCapacityUnits: 5
    AddressTableDynamoDBTable:
      Type: 'AWS::DynamoDB::Table'
      Properties:
        AttributeDefinitions:
          - AttributeName: estadoId
            AttributeType: S
          - AttributeName: cp
            AttributeType: N
          - AttributeName: municipioId
            AttributeType: S
          - AttributeName: colonia
            AttributeType: S
        KeySchema:
          - AttributeName: cp
            KeyType: HASH
          - AttributeName: colonia
            KeyType: RANGE
        ProvisionedThroughput:
          ReadCapacityUnits: 10
          WriteCapacityUnits: 10
        TableName: ${self:custom.addressTableName}
        GlobalSecondaryIndexes:
          - IndexName: stateId-municipalityId-index
            KeySchema:
              - AttributeName: estadoId
                KeyType: HASH
              - AttributeName: municipioId
                KeyType: RANGE
            Projection:
              ProjectionType: ALL
            ProvisionedThroughput:
              ReadCapacityUnits: 10
              WriteCapacityUnits: 10
