


///facebook user
const allowedParametersFacebook = ['firstName','lastName','photo','birthday','email','mobile','gender','hometown','ageRange','userId'];
let parameterAttributesFacebook = {
      firstName: {required: false, type: 'string'},
      lastName: {required: false, type: 'string'},
      photo: {required: false, type:'string'},
      userId: {required: true, type: 'string'},
      email: { required: false, type: 'string' },
      mobile: {required:false, type: 'string'},
      gender: {required: false, type: 'string'},
      birthday: {required:false,type: 'string'},
      hometown: {required:false,type:'string'},
      ageRange: {required:false,type:'string'}
}



let parameterAttributesFacebookSignUp= {
      firstName: {required:true, type: 'string'},
      lastName: {required:true, type: 'string'},
      photo: {required: true, type:'string'},
      userId: {required: true, type: 'string'},
      email: { required: false, type: 'string' },
      mobile: {required:false, type: 'string'},
      gender: {required: false, type: 'string'},
      birthday: {required:false,type: 'string'},
      hometown: {required:false,type:'string'},
      ageRange: {required:false,type:'string'}
}



let allowedParametersUpdate = ['userId','firstName','lastName','email','mobile','birthday']
let parameterAttributesUpdate = {
      firstName: { required: false, type: 'string' },
      lastName: { required: false, type: 'string' },
      mobile: { required: false, type: 'string' },
      birthday: { required: false, type: 'string' },
      email: { required: false, type: 'string' },
      userId: {required:true,type:'string'}
}




const getUpdateExpression = parameters =>{
    let res = "set "
    for(let ind in allowedParametersUpdate){
        let prm = allowedParametersUpdate[ind];
        if(prm in parameters){
            if(res.length>4)
                res += ","
            res += prm + " = :" + prm;

        }
    }
    return res.toString();
}



const getExpressionAttributeValues = parameters => {
    let res = {}

    for(let ind in allowedParametersUpdate){

        let prm = allowedParametersUpdate[ind]
        if(prm in parameters){
            let key = ":" + prm;
            res[key] = parameters[prm]

        }
     }
    return res;
}







module.exports = {
      parameterAttributesFacebook,
      allowedParametersFacebook,
      parameterAttributesFacebookSignUp,
      allowedParametersUpdate,
      parameterAttributesUpdate,
      getUpdateExpression,
      getExpressionAttributeValues
}
