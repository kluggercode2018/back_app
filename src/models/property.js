




///property

const allowedParameters = ['imagesUrls','propertyType','price','priceUnit','operation','propertyState','municipality','suburb','pc','street','externalNumber',
'lat','lng','constructionSize','landSize','bedrooms','bathrooms','halfBathrooms','parkingLots','description','userId'];


let parameterAttributes = {
    imagesUrls : {required:true, type: 'object'},
    propertyType: { required: true, type: 'string' },
    price: { required: true, type: 'number' },
    priceUnit: { required: true, type: 'string' },
    operation: { required: true, type: 'string' },
    propertyState: { required: true, type: 'string' },
    municipality: { required: true, type: 'string' },
    suburb: { required: true, type: 'string' },
    pc: { required: false, type: 'number' },
    street: { required: false, type: 'string' },
    parkingLots: { required: true, type: 'number' },
    bedrooms: { required: true, type: 'number' },
    bathrooms: { required: true, type: 'number' },
    constructionSize: { required: true, type: 'number' },
    externalNumber: { required: false, type: 'string' },
    lat: { required: true, type: 'number' },
    lng: { required: true, type: 'number' },
    landSize: { required: false, type: 'number' },
    halfBathrooms: { required: false, type: 'number' },
    userId : {required:true, type: 'string'},
    description: { required: false, type: 'string' }
}




















module.exports = {
      parameterAttributes,
      allowedParameters
}
