/***
  Module: Users
  author : Klugger
**/

const prm = require('./utils/parameters')// Helper function for checking multiple parameters
const string = require('./utils/string')
const User = require('./models/user')
const {checkToken} = require('./middlewares/auth')
const serverless = require('serverless-http')
const bodyParser = require('body-parser')
const express = require('express')
const uuidv5 = require('uuid/v5')
const AWS = require('aws-sdk')
const nodes2ts=require("nodes2ts")
const multer = require('multer')
const _ = require('underscore')
const jwt = require('jsonwebtoken')

let app = express()
let s3=new AWS.S3()


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())


// Dynamo tables
const PROPERTIES_TABLE = process.env.PROPERTIES_TABLE
const USERS_TABLE = process.env.USERS_TABLE
const USERS_IMAGES = process.env.USERS_IMAGES
const PROPERTIES_IMAGES=process.env.PROPERTIES_IMAGES
const IS_OFFLINE = process.env.IS_OFFLINE
const LIKES_TABLE = process.env.LIKES_TABLE



// Enable CORS

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


let dynamoDb
if (IS_OFFLINE === 'true') {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  });
} else
  dynamoDb = new AWS.DynamoDB.DocumentClient()





const URL_PROPERTIES_IMAGES = process.env.URL_PROPERTIES_IMAGES
const URL_USERS_IMAGES = process.env.URL_USERS_IMAGES









// function to get a property with format

/***
  Funcion: getPropertyFormat
  Input:  id
  Description: Function to return the info of a property with the format in the price
**/
 let getPropertyFormat = (id) =>{
     let params = {
         TableName: PROPERTIES_TABLE,
         KeyConditionExpression: "propertyId = :propertyId",
         ExpressionAttributeValues: {
             ":propertyId" : id
         }
     }


     return new Promise( (resolve,reject)=>{
        dynamoDb.query(params,(error,result)=>{
            if(error){
                console.log(error);
                reject(error)
            }else if(result.Items){
                result.Items[0].price = string.formatNumber(result.Items[0].price.toString());
                resolve(result.Items[0])
            }
        })
      })
}




/***
  Function: getPropertyFormat
  Description: Function to return the info of a property
  Input:  id
**/
 let getProperty = (id) =>{
     let params = {
         TableName: PROPERTIES_TABLE,
         KeyConditionExpression: "propertyId = :propertyId",
         ExpressionAttributeValues: {
             ":propertyId" : id
         }
     }


     return new Promise( (resolve,reject)=>{
        dynamoDb.query(params,(error,result)=>{
            if(error){
                console.log(error);
                reject(error)
            }else if(result.Items){
                resolve(result.Items[0])
            }
        })
      })
}





/***
  Function: getPropertyCoords
  Description: Function to return the coords of a property
  Input: id
**/
 let getPropertyCoords = (id) =>{
     let params = {
         TableName: PROPERTIES_TABLE,
         KeyConditionExpression: "propertyId = :propertyId",
         ProjectionExpression: "lat,lng,propertyId",
         ExpressionAttributeValues: {
             ":propertyId" : id
         }
     }


     return new Promise( (resolve,reject)=>{
        dynamoDb.query(params,(error,result)=>{
            if(error){
                console.log(error);
                reject(error)
            }else if(result.Items){
                resolve(result.Items[0])
            }
        })
      })
}




/***
  Function: getUser
  Description: Function to return the info of a user
  Input:  user id
**/
let getUser = (id)=>{
  let params = {
      TableName : USERS_TABLE,
      IndexName: "id-index",
      KeyConditionExpression: "id = :id",
      ExpressionAttributeValues:{
          ":id" : id
      }
  }

  return new Promise( (resolve,reject)=>{
    dynamoDb.query(params, (error, result) => {
        if(error){
            return reject(error)
        }else{
            return resolve(result.Items[0])
        }
    })
  })
}






/***
  Function: getAllProperties
  Description: Function to return all the properties of a user
  Input:  userId
**/


let getAllProperties =(userId)=>{
  let params = {
      TableName : PROPERTIES_TABLE,
      IndexName: 'userId-index',
      KeyConditionExpression: "userId = :userId",
      ExpressionAttributeValues:{
          ":userId" : userId
      }
  }

  return new Promise( (resolve,reject)=>{

        let items = [];

        function onQuery(err, data) {
          if (err) {
              console.log(err)
              return reject(err)
          } else {
              console.log("Query succeeded.");
              items = items.concat(data.Items);
              if (typeof data.LastEvaluatedKey != "undefined") {
                  console.log("Querying for more...");
                  params.ExclusiveStartKey = data.LastEvaluatedKey;
                  dynamoDb.query(params, onQuery);
              } else {
                  return resolve(items)
              }
          }
        }

        dynamoDb.query(params, onQuery);
  })
}



//Ping endpoint for testing purposes
app.get('/users/ping', (req, res) => {
  res.json( {message: 'pong'} );
});


//https://developers.google.com/identity/sign-in/web/sign-in
//Google





/***
  Function: http post method
  Description: Function to register a new user with the facebook id
**/
app.post('/users/facebook' ,(req, res) => {
    let parameters = _.pick(req.body,User.allowedParametersFacebook)
    let errorMessage =  prm.checkParameters(parameters, User.parameterAttributesFacebook)

    if (errorMessage) {
        console.log(errorMessage)
        return res.status(400).json(errorMessage)
    }

    let params = {
        TableName : USERS_TABLE,
        KeyConditionExpression: "providerId = :providerId",
        ExpressionAttributeValues: {
            ":providerId": parameters.userId
        }
    }

     dynamoDb.query(params, (error, result) => {
         if(error){
             console.log(error)
             return res.status(500).json({error})
         }else if(result.Items[0]){
             let token = jwt.sign({
                 user: result.Items[0]
             }, "semilla-para-token-klugger", { expiresIn: process.env.EXPIRE_TOKEN })


             let response = {
                 user: result.Items[0],
                 token
             }

             return res.json(response)
         }else{

               let errorMessageSignUp =  prm.checkParameters(parameters, User.parameterAttributesFacebookSignUp)
               if (errorMessageSignUp) {
                   console.log(errorMessageSignUp)
                   return res.status(400).json(errorMessageSignUp)
               }

               let paramsString = JSON.stringify(parameters.userId)
               let userId = uuidv5(paramsString, '1b671a64-40d5-491e-99b0-da01ff1f3341')
               parameters.providerId = parameters.userId
               parameters.id = userId
               delete parameters.userId

               let params = {
                   TableName: USERS_TABLE,
                   Item: parameters
               }

               /// callback nested
               dynamoDb.put(params, (err, data) => {
                   if (err) {
                       console.log(err)
                       return res.status(400).json({ error:err})
                   }else{
                        //crear token
                       let token = jwt.sign({
                           user: parameters
                       }, "semilla-para-token-klugger", { expiresIn: process.env.EXPIRE_TOKEN })

                       let response = {
                           user : parameters,
                           token
                       }
                       return res.json(response)
                   }
               })

      }})

});



/***
  Function: http post method
  Description: Function to remove a like of  a property
  Input: propertyId,userId
  Output: verified message
**/
app.post('/users/like/undo',(req,res)=>{
  let allowedParameters = ['propertyId','userId']
  let parameters = _.pick(req.body,allowedParameters)
  let parameterAttributes= {
        propertyId: {required: true, type: 'string'},
        userId: {required: true, type: 'string'}
  }

  let errorMessage =  prm.checkParameters(parameters,parameterAttributes)

  if (errorMessage) {
      console.log(errorMessage)
      return res.status(400).json(errorMessage)
  }


  let paramsUpdate = {
      TableName:LIKES_TABLE,
      Key:{
          "propertyId" : parameters.propertyId,
          "userId": parameters.userId
      },
      UpdateExpression: "set statusProperty = :statusProperty",
      ExpressionAttributeValues:{
          ":statusProperty": null
      }
  };


  getProperty(parameters.propertyId)
  .then( (property)=>{
      if(property==null)
          return res.status(400).json({error:"property does not exist"});

      getUser(parameters.userId)
      .then( user=>{
          if(user==null)
            return res.status(400).json({error:"user does not exist"})

          dynamoDb.update(paramsUpdate, function(err, dataUpdated) {
              if (err) {
                  console.log(err)
                  return res.status(500).json({error:err})
              } else {
                  let response = {
                      message: "like removed",
                      propertyId: req.body.propertyId
                  }
                  return res.json(response)
              }
          })
      })
  })
})

















/***
  Function: http post method
  Description: Function to remove a dislike of  a property
  Input: propertyId,userId
  Output: verified message
**/
app.post('/users/dislike/undo',(req,res)=>{
  let allowedParameters = ['propertyId','userId']
  let parameters = _.pick(req.body,allowedParameters)
  let parameterAttributes= {
        propertyId: {required: true, type: 'string'},
        userId: {required: true, type: 'string'}
  }

  let errorMessage =  prm.checkParameters(parameters,parameterAttributes)

  if (errorMessage) {
      console.log(errorMessage)
      return res.status(400).json(errorMessage)
  }

  let paramsUpdate = {
      TableName:LIKES_TABLE,
      Key:{
          "propertyId" : parameters.propertyId,
          "userId": parameters.userId
      },
      UpdateExpression: "set statusProperty = :statusProperty",
      ExpressionAttributeValues:{
          ":statusProperty":null
      }
  };



  getProperty(parameters.propertyId)
  .then( (property)=>{
      if(property==null)
          return res.status(400).json({error:"property does not exist"});

      getUser(parameters.userId)
      .then( user=>{
          if(user==null)
            return res.status(400).json({error:"user does not exist"})

          dynamoDb.update(paramsUpdate, function(err, dataUpdated) {
              if (err) {
                  console.log(err)
                  return res.status(500).json({error:err})
              } else {
                  let response = {
                      message: "dislike removed",
                      propertyId: req.body.propertyId
                  }
                  return res.json(response)
              }
          })
      })
  })

})







/***
  Function: http post method
  Description: Function to add a property in likes
  Input: propertyId,userId
  Output: verified message
**/
app.post('/users/like',(req,res)=>{
  let allowedParameters = ['propertyId','userId']
  let parameters = _.pick(req.body,allowedParameters)
  let parameterAttributes= {
        propertyId: {required: true, type: 'string'},
        userId: {required:true, type: 'string'}
  }

  let errorMessage =  prm.checkParameters(parameters,parameterAttributes)

  if (errorMessage) {
      console.log(errorMessage)
      return res.status(400).json(errorMessage)
  }
  getProperty(parameters.propertyId)
  .then( (property)=>{
      if(property==null)
          return res.status(400).json({error:"property does not exist"});

      getUser(parameters.userId)
      .then( user=>{
          if(user==null)
              return res.status(400).json({error:"users does not exist"});

              parameters.statusProperty = "LIKE"
              let paramsUpdate = {
                  TableName:LIKES_TABLE,
                  Key:{
                      "propertyId" : parameters.propertyId,
                      "userId": parameters.userId
                  },
                  UpdateExpression: "set statusProperty = :statusProperty",
                  ExpressionAttributeValues:{
                      ":statusProperty":parameters.statusProperty
                  }
              };

              dynamoDb.update(paramsUpdate, function(err, dataUpdated) {
                  if (err) {
                      console.log(err)
                      return res.status(500).json({error:err})
                  } else {
                      let response = {
                          message: "property saved",
                          propertyId: req.body.propertyId
                      }
                      return res.json(response)
                  }
              })
      })

  })
})








/***
  Function: http post method
  Description: Function to add a property in dislikes
  Input: propertyId,userId
  Output: verified message
**/
app.post('/users/dislike',(req,res)=>{
  let allowedParameters = ['propertyId','userId']
  let parameters = _.pick(req.body,allowedParameters)
  let parameterAttributes= {
        propertyId: {required: true, type: 'string'},
        userId: {required: true, type: 'string'}
  }

  let errorMessage =  prm.checkParameters(parameters,parameterAttributes)

  if (errorMessage) {
      console.log(errorMessage)
      return res.status(400).json(errorMessage)
  }


  getProperty(parameters.propertyId)
  .then( (property)=>{
      if(property==null)
          return res.status(400).json({error:"property does not exist"});

      getUser(parameters.userId)
      .then( user=>{
          if(user==null)
              return res.status(400).json({error:"users does not exist"});

              parameters.statusProperty = "DISLIKE"
              let paramsUpdate = {
                  TableName:LIKES_TABLE,
                  Key:{
                      "propertyId" : parameters.propertyId,
                      "userId": parameters.userId
                  },
                  UpdateExpression: "set statusProperty = :statusProperty",
                  ExpressionAttributeValues:{
                      ":statusProperty":parameters.statusProperty
                  }
              };

              dynamoDb.update(paramsUpdate, function(err, dataUpdated) {
                  if (err) {
                      console.log(err)
                      return res.status(500).json({error:err})
                  } else {
                      let response = {
                          message: "property saved",
                          propertyId: req.body.propertyId
                      }
                      return res.json(response)
                  }
              })
      })

  })
})



















/***
  Function: http post method
  Description: to get all the user liked properties to show in the map
  Input: propertyId,userId
  Output: list of properties {id,coords}
**/
app.get('/users/likes/map/:userId',(req, res) => {
    let userId = req.params.userId
    let params = {
        TableName : LIKES_TABLE,
        KeyConditionExpression: "userId = :userId",
        FilterExpression: "statusProperty = :statusProperty",
        ProjectionExpression: "propertyId",
        ExpressionAttributeValues:{
            ":userId": userId,
            ":statusProperty": "LIKE"
        }
    }

    getUser(userId)
    .then(data=>{
        if(data==null)
            return res.status(400).json({error:'users does not exist'})

        dynamoDb.query(params, (error, result) => {
            if(error){
                console.log(error)
                return {error:error}
            }else{
                return {message:result.Items}
            }
         }).promise()
         .then( (dataRecieved)=>{

            if(dataRecieved.Items===undefined){
                return res.status(500).json({error:dataRecieved.error});
            }
            let responseItems = dataRecieved.Items;
            let promises = []
            for(let i in responseItems){
                let propertyId = responseItems[i].propertyId
                promises.push(getPropertyCoords(propertyId))
            }

            Promise.all(promises)
            .then( (values)=>{
                return res.json(values)
            })
            .catch( (error)=>{
                console.log(error);
                return res.status(500).json({error})
            })

         })
   })
   .catch( error => {
      return res.status(500).json({error})
   })

});





/***
  Function: http get
  Description: to get all the user liked properties
  Output: list of properties
**/
app.get('/users/likes/:userId',(req, res) => {
    let userId = req.params.userId
    let params = {
        TableName : LIKES_TABLE,
        KeyConditionExpression: "userId = :userId",
        FilterExpression: "statusProperty = :statusProperty",
        ExpressionAttributeValues:{
            ":userId": userId,
            ":statusProperty": "LIKE"
        }
    }

    getUser(userId)
    .then(data=>{
        if(data==null)
            return res.status(400).json({error:'users does not exist'})

        dynamoDb.query(params, (error, result) => {
            if(error){
                console.log(error)
                return {error:error}
            }else{
                return {message:result.Items}
            }
         }).promise()
         .then( (dataRecieved)=>{

            if(dataRecieved.Items===undefined){
                return res.status(500).json({error:dataRecieved.error});
            }
            let responseItems = dataRecieved.Items;
            let promises = []
            for(let i in responseItems){
                let propertyId = responseItems[i].propertyId
                promises.push(getPropertyFormat(propertyId))
            }

            Promise.all(promises)
            .then( (values)=>{
                return res.json(values)
            })
            .catch( (error)=>{
                console.log(error);
                return res.status(500).json({error})
            })

         })
   })
   .catch( error => {
      return res.status(500).json({error})
   })

});





/***
  Function: http get
  Description: to get all the user disliked properties
  Output: list of properties
**/
app.get('/users/dislikes/:userId',(req, res) => {
    let userId = req.params.userId
    let params = {
        TableName : LIKES_TABLE,
        KeyConditionExpression: "userId = :userId",
        FilterExpression: "statusProperty = :statusProperty",
        ExpressionAttributeValues:{
            ":userId": userId,
            ":statusProperty": "DISLIKE"
        }
    }



    getUser(userId)
    .then(data=>{
        if(data==null)
            return res.status(400).json({error:'users does not exist'})
        dynamoDb.query(params, (error, result) => {
            if(error){
                console.log(error)
                return {error:error}
            }else{
                return {message:result.Items}
            }
         }).promise()
         .then( dataRecieved =>{
            if(dataRecieved.Items ===undefined)
              return res.status(400).json({error:dataRecieved.error})

            let responseItems = dataRecieved.Items;
            let promises = []
            for(let i in responseItems){
                let propertyId = responseItems[i].propertyId
                promises.push(getProperty(propertyId))
            }

            Promise.all(promises)
            .then( (values)=>{
                return res.json(values)
            })
            .catch( error=>{
                console.log(error);
                return res.status(500).json(error)
            })

         })
       })
       .catch( error=>{
            return res.status(500).json({error});
       })

});




/***
  Function: http get
  Description: to get all the user  properties (owner)
  Output: list of properties
**/

app.get('/users/properties/:userId',(req,res)=>{
    let userId = req.params.userId
    getUser(userId)
    .then(data=>{
        if(data==null)
            return res.status(400).json({error:'users does not exist'})
        getAllProperties(userId)
        .then( (data)=>{
            return res.json(data);
        })
        .catch( (error)=>{
            return res.status(500).json({error})
        })
    })
    .catch( error=>{
        return res.status(500).json({error});
    })
})



/*
app.post('/users/update',checkToken,(req,res)=>{
    let id = req.user.id
    let providerId = req.user.providerId

    req.body.userId = req.user.id

    let parameters = _.pick(req.body,User.allowedParametersUpdate)
    let errorMessage = prm.checkParameters(parameters,User.parameterAttributesUpdate)


    if (errorMessage) {
      console.log(errorMessage);
      return res.status(400).json(errorMessage);
    }

    let UpdateExpressionTemp = User.getUpdateExpression(parameters);
    let ExpressionAttributeValuesTemp = User.getExpressionAttributeValues(parameters)

    if(UpdateExpressionTemp.length==4)
        return res.json(parameters)

    let paramsUpdate = {
        TableName: USERS_TABLE,
        Key:{
          "id" : id,
          "providerId": providerId
        },
        UpdateExpression: UpdateExpressionTemp,
        ExpressionAttributeValues: ExpressionAttributeValuesTemp,
        ReturnValues: "UPDATED_NEW"
    }

    dynamoDb.update(paramsUpdate, (error,data)=>{
          if(error)
            return res.status(500).json({error})
          else
            return res.json(data)
    })





})

*/


//
// app.post('/users/new/test', (req, res) => {
//     let parameters = req.body
//     let params = {
//         TableName : USERS_TABLE,
//         Item: parameters
//     }
//     dynamoDb.put(params, (error, data) => {
//         if (error) {
//             console.log(error)
//             return res.status(500).json({error})
//         }else{
//             let response = {
//                 message: "user inserted",
//                 propertyId: parameters.propertyId
//             }
//             return res.json(response)
//         }
//     })
// })


module.exports.handler = serverless(app);
