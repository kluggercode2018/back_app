
/***
  Module: Addresses
  author : Klugger
**/

let serverless = require('serverless-http');
let bodyParser = require('body-parser');
let express = require('express');
let AWS = require('aws-sdk');
let app = express();



let prm = require('./utils/parameters');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});




// Addresses table
let ADDRESSES_TABLE = process.env.ADDRESSES_TABLE;




//functions to compare two string
function change(character){
  if(character=='á')return 'a';
  if(character=='é')return 'e';
  if(character=='í')return 'i';
  if(character=='ó')return 'o';
  if(character=='ú')return 'u';
  return character;
}
function compareStrings(a,b){
  var $a=a.toString().toLowerCase();
  var $b=b.toString().toLowerCase();
  var len_a=$a.length;
  var len_b=$b.length;
  var len=Math.min(len_a,len_b);
  for(var i=0;i<len;i++){
    x=change($a[i]);
    y=change($b[i]);
    if(x<y)return -1;
    else if(y<x)return 1;
  }
  return (len_a-len_b);
}


// Try an offline DynamoDB connection
var IS_OFFLINE = process.env.IS_OFFLINE;
var dynamoDb;
if (IS_OFFLINE === 'true') {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'us-east-1',
    endpoint: 'https://dynamodb.us-east-1.amazonaws.com'
  });
} else {
  dynamoDb = new AWS.DynamoDB.DocumentClient();
}




// Ping endpoint for testing purposes
app.get('/addresses/ping', (req, res) => {
  res.json( {message: 'pong'} );
});





//query municipalitiesId-> (cp,colonia)

/***
  Function: http post method
  Description: Method to get all the suburs and code
  Output: verified message
  Input: email
**/

app.post('/addresses/neighborhood', (req, res) => {
  let parameters = req.body;
  let parameterAttributes = {
    municipioId: { required: true, type: 'string' },
    estadoId: { required: true, type: 'string' }
  }

  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

  if (errorMessage) {
    console.log(errorMessage);
    return res.status(400).json(errorMessage);
  }
  let params = {
    TableName : ADDRESSES_TABLE,
    IndexName: "stateId-municipalityId-index",
    KeyConditionExpression: "estadoId = :edo and municipioId=:mncp",
    ProjectionExpression: "colonia, cp",
    ExpressionAttributeValues: {
        ":mncp":parameters.municipioId,
        ":edo":parameters.estadoId
    }
  };

  var items = [];

  function onQuery(err, data) {
    if (err) {
        console.log('error');
    } else {
        console.log("Query succeeded.");
        items = items.concat(data.Items);
        console.log(items.length);
        if (typeof data.LastEvaluatedKey != "undefined") {
          console.log("Querying for more...");
          params.ExclusiveStartKey = data.LastEvaluatedKey;
          dynamoDb.query(params, onQuery);
        } else {
          items.sort(function(a, b) {
            var value=compareStrings(a.colonia,b.colonia);
            return (value!==0)?value:(parseFloat(a.cp)-parseFloat(b.cp));
          });
          return res.json(items);
        }
    }
  }

  dynamoDb.query(params, onQuery);
});


//query municipalitiesId-> (cp,colonia)

/***
  Function: http post method
  Description: Method to get all the suburs and code(app)
  Output: list of suburbs
**/

app.post('/addresses/neighborhood/react', (req, res) => {
  let parameters = req.body;
  let parameterAttributes = {
    municipioId: { required: true, type: 'string' },
    estadoId: { required: true, type: 'string' }
  }

  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

  if (errorMessage) {
    console.log(errorMessage);
    return res.status(400).json(errorMessage);
  }

  let params = {
    TableName : ADDRESSES_TABLE,
    IndexName: "stateId-municipalityId-index",
    KeyConditionExpression: "estadoId = :edo and municipioId=:mncp",
    ProjectionExpression: "colonia, cp",
    ExpressionAttributeValues: {
        ":mncp":parameters.municipioId,
        ":edo":parameters.estadoId
    }
  };

  var items = [];

  function onQuery(err, data) {
    if (err) {
        console.log('error');
    } else {
        console.log("Query succeeded.");
        items = items.concat(data.Items);
        console.log(items.length);
        if (typeof data.LastEvaluatedKey != "undefined") {
          console.log("Querying for more...");
          params.ExclusiveStartKey = data.LastEvaluatedKey;
          dynamoDb.query(params, onQuery);
        } else {
          for(let i=0;i<items.length;i++){
            items[i].label  = items[i].colonia;
            items[i].value  = items[i].colonia;
            delete items[i].colonia;
          }
          items.sort(function(a, b) {
            var value=compareStrings(a.label,b.label);
            return (value!==0)?value:(parseFloat(a.cp)-parseFloat(b.cp));
          });
          return res.json(items);
        }
    }
  }

  dynamoDb.query(params, onQuery);
});







/***
  Function: http post method
  Description: Method to get all the municipalities
  Input: state
  Output: list of municipalities
**/
app.post('/addresses/municipalities', (req, res) => {
  let parameters = req.body;
  let parameterAttributes = {
    estadoId: { required: true, type: 'string' }
  }

  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);
  if (errorMessage) {
    console.log(errorMessage);
    return res.status(400).json(errorMessage);
  }

  let params = {
    TableName : ADDRESSES_TABLE,
    IndexName: 'stateId-municipalityId-index',
    KeyConditionExpression: "estadoId = :estadoId",
    ProjectionExpression: "municipio , municipioId",
    ExpressionAttributeValues: {
        ":estadoId":parameters.estadoId
    }
  };


  let items = [];

  function onQuery(err, data) {
    if (err) {
        console.log('error');
    } else {
        console.log("Query succeeded.");
        items = items.concat(data.Items);
        if (typeof data.LastEvaluatedKey != "undefined") {
          console.log("Querying for more...");
          params.ExclusiveStartKey = data.LastEvaluatedKey;
          dynamoDb.query(params, onQuery);
        } else {
          let array=[];
          let mySet=new Set();
          for(let i=0;i<items.length;i++){
            if(mySet.has(items[i]['municipioId'])==false){
                array.push({municipio:items[i]['municipio'],municipioId:items[i]['municipioId']});
                mySet.add(items[i]['municipioId']);
            }
          }
          array.sort(function(a, b) {
              return compareStrings(a.municipio,b.municipio);
          });
          return res.json(array)
        }
    }
  }
  dynamoDb.query(params, onQuery)
});






module.exports.handler = serverless(app);
