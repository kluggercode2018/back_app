const jwt = require('jsonwebtoken');


// =====================
// Verificar Token
// =====================
let checkToken = (req, res, next) => {
    let token = req.headers.authorization

    jwt.verify(token,"semilla-para-token-klugger", (error, decoded) => {
        if (error)
            return res.status(400).json({message: "missing authentication token"})
        req.user = decoded.user
        next()
    });
};


module.exports = {
    checkToken
}
