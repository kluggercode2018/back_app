
/***
  Module: Properties
  author : Klugger
**/

const string = require('./utils/string')
const gmail = require('./utils/gmail')
const prm = require('./utils/parameters')// Helper function for checking multiple parameters
const serverless = require('serverless-http')
const bodyParser = require('body-parser')
const express = require('express')
const ejs = require('ejs')
const Property = require('./models/property')
const fs = require('fs')
const uuidv5 = require('uuid/v5')
const AWS = require('aws-sdk')
const multer = require('multer')
const app = express()
const DateDiff = require('date-diff')
const _ = require('underscore')
const s3=new AWS.S3()
const {checkToken} = require('./middlewares/auth')



//const nodes2ts=require("nodes2ts") geo table
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// environment variables
const PROPERTIES_TABLE = process.env.PROPERTIES_TABLE
const USERS_TABLE = process.env.USERS_TABLE
const LIKES_TABLE = process.env.LIKES_TABLE
const PROPERTIES_IMAGES = process.env.PROPERTIES_IMAGES
const IS_OFFLINE = process.env.IS_OFFLINE






// Try an offline DynamoDB connection
let dynamoDb;
if (IS_OFFLINE === 'true') {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  });
} else
  dynamoDb = new AWS.DynamoDB.DocumentClient();








// function to get a property
 let getProperty = (id) =>{

     let params = {
         TableName: PROPERTIES_TABLE,
         KeyConditionExpression: "propertyId = :propertyId",
         ExpressionAttributeValues: {
             ":propertyId" : id
         }
     }


     return new Promise( (resolve,reject)=>{
        dynamoDb.query(params,(error,result)=>{
            if(error){
                console.log(error);
                return reject(error)
            }else if(result.Items){
                return resolve(result.Items[0])
            }
        })
      })
}



let getUser = (id)=>{
  let params = {
      TableName : USERS_TABLE,
      IndexName: "id-index",
      KeyConditionExpression: "id = :id",
      ExpressionAttributeValues:{
          ":id" : id
      }
  }

  return new Promise( (resolve,reject)=>{
    dynamoDb.query(params, (error, result) => {
        if(error){
            return reject(error)
        }else{
            return resolve(result.Items[0])
        }
    })
  })
}




let getDislikes = (propertyId)=>{
     let params = {
         TableName : LIKES_TABLE,
         IndexName: "propertyId-index",
         KeyConditionExpression: "propertyId = :propertyId",
         ProjectionExpression: "userId",
         FilterExpression: "statusProperty = :statusProperty",
         ExpressionAttributeValues:{
             ":propertyId" : propertyId,
             ":statusProperty" : "DISLIKE"
         }
     }

     return new Promise( (resolve,reject)=>{

           let items = [];

           function onQuery(err, data) {
             if (err) {
                 console.log(err);
                 return reject(err)
             } else {
                 console.log("Query succeeded.");
                 items = items.concat(data.Items);
                 if (typeof data.LastEvaluatedKey != "undefined") {
                     console.log("Querying for more...");
                     params.ExclusiveStartKey = data.LastEvaluatedKey;
                     dynamoDb.query(params, onQuery);
                 } else {
                    return resolve(items)
                 }
             }
           }

           dynamoDb.query(params, onQuery);
     })
}


let getLikes = (propertyId)=>{
     let params = {
         TableName : LIKES_TABLE,
         IndexName: "propertyId-index",
         KeyConditionExpression: "propertyId = :propertyId",
         ProjectionExpression: "userId",
         FilterExpression: "statusProperty = :statusProperty",
         ExpressionAttributeValues:{
             ":propertyId" : propertyId,
             ":statusProperty" : "LIKE"
         }
     }

     return new Promise( (resolve,reject)=>{

           let items = [];

           function onQuery(err, data) {
             if (err) {
                 console.log(err);
                 return reject(err)
             } else {
                 console.log("Query succeeded.");
                 items = items.concat(data.Items);
                 if (typeof data.LastEvaluatedKey != "undefined") {
                     console.log("Querying for more...");
                     params.ExclusiveStartKey = data.LastEvaluatedKey;
                     dynamoDb.query(params, onQuery);
                 } else {
                    return resolve(items)
                 }
             }
           }

           dynamoDb.query(params, onQuery);
     })
}

// function to get a property
let getLikesDislikes = (propertyId) =>{

   let params = {
       TableName : LIKES_TABLE,
       IndexName: "propertyId-index",
       KeyConditionExpression: "propertyId = :propertyId",
       ProjectionExpression: "userId",
       ExpressionAttributeValues:{
           ":propertyId" : propertyId
       }
   }

   return new Promise( (resolve,reject)=>{

         let items = [];

         function onQuery(err, data) {
           if (err) {
               console.log(err);
               return reject(err)
           } else {
               console.log("Query succeeded.");
               items = items.concat(data.Items);
               if (typeof data.LastEvaluatedKey != "undefined") {
                   console.log("Querying for more...");
                   params.ExclusiveStartKey = data.LastEvaluatedKey;
                   dynamoDb.query(params, onQuery);
               } else {
                  return resolve(items)
               }
           }
         }

         dynamoDb.query(params, onQuery);
   })
}



// Ping endpoint for testing purposes
app.get('/properties/ping', (req, res) => {
  res.json( {message: 'pong'} );
});




// to post a property
/*API Para que un usuario pubique una propiedad (require autentificación)
checar si existe el usuario
*/



/***
  Function: http post method
  Description: Method to add new property   (app)
  Input: [list of parameters, inclued userId]
  Output: verified message and new propertyId
**/


app.post('/properties/new' ,(req, res) => {
    let parameters = _.pick(req.body,Property.allowedParameters)
    let errorMessage =  prm.checkParameters(parameters, Property.parameterAttributes)

    if (errorMessage) {
        console.log(errorMessage)
        return res.status(400).json(errorMessage)
    }

    let params = {
        TableName : PROPERTIES_TABLE,
        Item: parameters
    }

    let paramsString = JSON.stringify(parameters)
    let propertyId = uuidv5(paramsString, '1b671a64-40d5-491e-99b0-da01ff1f3341')
    parameters.propertyId = propertyId

    dynamoDb.put(params, (error, data) => {
        if (error) {
            console.log(error)
            return res.status(400).json({error})
        }else{
            let response = {
                message: "property inserted",
                propertyId
            }
            return res.json(response)
        }
    })
})






/***
  Function: http post method
  Description: Method to add new property   (web | dashboard)
  Input: [list of parameters, inclued userId]
  Output: verified message and new propertyId
**/
app.post('/properties/upload/property',(req, res)=> {
  let parameters = req.body;
  let parameterAttributes = {
    imagesUrls : {required:true, type: 'object'},
    propertyType: { required: true, type: 'string' },
    price: { required: true, type: 'number' },
    priceType: { required: true, type: 'boolean' },
    priceUnit: { required: true, type: 'string' },
    operation: { required: true, type: 'string' },
    propertyState: { required: true, type: 'string' },
    municipality: { required: true, type: 'string' },
    suburb: { required: true, type: 'string' },
    pc: { required: true, type: 'number' },
    street: { required: true, type: 'string' },
    externalNumber: { required: false, type: 'string' },
    internalNumber: { required: false, type: 'string' },
    externalNumberVisibility: { required: true, type: 'boolean' },
    internalNumberVisibility: { required: true, type: 'boolean' },
    lat: { required: true, type: 'number' },
    lng: { required: true, type: 'number' },
    constructionSize: { required: true, type: 'number' },
    constructionSizeUnit: { required: true, type: 'string' },
    landSize: { required: true, type: 'number' },
    landSizeUnit: { required: true, type: 'string' },
    bedrooms: { required: false, type: 'number' },
    bathrooms: { required: false, type: 'number' },
    halfBathrooms: { required: false, type: 'number' },
    parkingLots: { required: false, type: 'number' },
    antiqueness: { required: true, type: 'number' },
    floors: { required: false, type: 'number' },
    gardenSize: { required: false, type: 'number' },
    gardenSizeUnit: { required: true, type: 'string' },
    air: { required: true, type: 'boolean' },
    elevator: { required: true, type: 'boolean' },
    gym: { required: true, type: 'boolean' },
    heat: { required: true, type: 'boolean' },
    laundry: { required: true, type: 'boolean' },
    swimmingPool: { required: true, type: 'boolean' },
    security: { required: true, type: 'boolean' },
    roofgarden: { required: true, type: 'boolean' },
    phone: { required: true, type: 'number' },
    mail: { required: true, type: 'string' },
    description: { required: false, type: 'string' },
    propertyId: {required: true, type: 'string'},
    //userId: {required:false , type: 'string'},
    pageSource: {required:true, type: 'string'}
  };
  parameters.publicationDate = string.formatDate();
  parameters.userId = "temporal";
  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);
  if (errorMessage) {
    console.log(errorMessage);
    return res.status(400).json(errorMessage);
  }


  let params = {
      TableName : PROPERTIES_TABLE,
      Item: parameters
  }


  dynamoDb.put(params, (error, data) => {
      if (error) {
          console.log(error)
          return res.status(400).json({error})
      }else{
          let response = {
              message: "property inserted"
          }
          return res.json(response)
      }
  })


});



app.post('/properties/new/test', (req, res) => {
    let parameters = req.body
    let params = {
        TableName : PROPERTIES_TABLE,
        Item: parameters
    }


    let paramsString = JSON.stringify(parameters)
    let propertyId = uuidv5(paramsString, '1b671a64-40d5-491e-99b0-da01ff1f3341')
    parameters.propertyId = propertyId

    dynamoDb.put(params, (error, data) => {
        if (error) {
            console.log(error)
            return res.status(500).json({error})
        }else{
            let response = {
                message: "property inserted",
                propertyId: parameters.propertyId
            }
            return res.json(response)
        }
    })
})









/***
  Function: http post method
  Description: Method to erase a new property   (app)
  Input: userId,propertyId
  Output: verified message and new propertyId
**/

app.post('/properties/erase', (req, res) => {
    let parameters = _.pick(req.body,['propertyId','userId'])
    let parameterAttributes= {
          propertyId: {required: true, type: 'string'},
          userId: {required: true, type: 'string'}
    }

    let errorMessage =  prm.checkParameters(parameters, parameterAttributes)
    if (errorMessage) {
        console.log(errorMessage)
        return res.status(400).json(errorMessage)
    }

    let params = {
        TableName : PROPERTIES_TABLE,
        Item: parameters
    }

    getProperty(parameters.propertyId)
    .then( prop => {
        if(prop==null)
          return res.status(400).json({error:"property does not exist"})
        if(prop.userId != parameters.userId)
            return res.status(400).json({error:'permission denied'})
        getLikesDislikes(parameters.propertyId)
        .then( usersid =>{
            let promises = []
            for(let id in usersid){
                let paramsDelete = {
                  TableName: LIKES_TABLE,
                  Key:{
                      "userId" : usersid[id].userId,
                      "propertyId": parameters.propertyId
                  },
                }

                promises.push(dynamoDb.delete(paramsDelete,(err, data)=> { if(err)console.log(err);}).promise());
            }

            Promise.all(promises)
            .then(()=>{
                let paramsDelete = {
                  TableName: PROPERTIES_TABLE,
                  Key:{
                    'propertyId' : prop.propertyId,
                    'operation': prop.operation
                  }
                }

                dynamoDb.delete(paramsDelete, function(error, data) {
                    if (error)
                        return res.status(500).json({error});
                     else{
                       let response = {
                              message: "property erased",
                          propertyId: prop.propertyId
                       }
                       return res.json(response)
                     }
                })
            })
            .catch( (error)=>{
                return res.status(500).json({error})
            })
        })
    }).catch( error=>{
        console.log(error);
        return res.status(400).json({error})
    })

})





app.get('/properties/likesanddislikes/:propertyId',(req,res)=>{
  getLikesDislikes(req.params.propertyId)
  .then(data=>{
    res.json(data);
  })
})





/***
  Function: http get method
  Description: Method to show all the user that likes a property
  Output: list of users
**/
app.get('/properties/likes/:propertyId',(req,res)=>{
  let propertyId = req.params.propertyId
  getProperty(propertyId)
  .then( (data)=>{
      if(data==null)
        return res.status(400).json({error:"property does not exist"})
      getLikes(propertyId)
      .then( usersid =>{
            let promises = []
            for(let id in usersid){
                promises.push(getUser(usersid[id].userId));
            }


            Promise.all(promises)
            .then( (values)=>{
                let others = values.filter( value =>{
                    return value!=null
                })
                return res.json(others)
            })
            .catch( (error)=>{
                console.log(error);
                return res.status(500).json({error})
            })
      })
  }).catch( (error)=>{
      return res.status(400).json({error})
  })
})








/***
  Function: http get method
  Description: Method to show all the user that dislikes a property
  Output: list of users
**/

app.get('/properties/dislikes/:propertyId',(req,res)=>{
  let propertyId = req.params.propertyId
  getProperty(propertyId)
  .then( (data)=>{
      if(data==null)
        return res.status(400).json({error:"property does not exist"})
      getDislikes(propertyId)
      .then( usersid =>{
            let promises = []
            for(let id in usersid){
                promises.push(getUser(usersid[id].userId));
            }
            Promise.all(promises)
            .then( (values)=>{
                let others = values.filter( value =>{
                    return value!=null
                })
                return res.json(others)
            })
            .catch( (error)=>{
                console.log(error);
                return res.status(500).json({error})
            })
      })
  }).catch( (error)=>{
      return res.status(400).json({error})
  })
})







/***
  Function: http post  method
  Description: Method to get the property info
  Output: property info
**/
app.post("/properties/get/:id",(req,res)=>{

    let parameters = req.body;
    let id = req.params.id
    let parameterAttributes= {
          userId: {required: true, type: 'string'}
    }
    let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

    if (errorMessage) {
        console.log(errorMessage)
        return res.status(400).json(errorMessage)
    }

    getUser(parameters.userId)
    .then (userData =>{
        if(userData==null)
            return res.status(400).json({error:'users does not exist'});
        let userId = parameters.userId;
        getProperty(id)
        .then( (data)=>{
            if(data==null)
              return res.status(400).json({error:"property does not exist"})
            let paramsUpdate = {
                TableName:LIKES_TABLE,
                Key:{
                    "propertyId" : data.propertyId,
                    "userId": userId
                },
                UpdateExpression: "set seen = :seen",
                ExpressionAttributeValues:{
                    ":seen":true
                }
            };

            dynamoDb.update(paramsUpdate, function(err, dataUpdated) {
                if (err) {
                    console.log(err)
                    return res.status(500).json({error:err})
                } else {
                    data.price = string.formatNumber(data.price.toString());
                    return res.json(data)
                }
            });

        }).catch( (error)=>{
            return res.status(400).json({error})
        })
  })
})






/***
  Function: http get  method
  Description: Method to get all the properties in sell
  Output: list of properties
**/
app.get('/properties/sell',(req, res) => {

    let params = {
        TableName : PROPERTIES_TABLE,
        IndexName: "operation-index",
        KeyConditionExpression: "operation = :operation",
        ExpressionAttributeValues:{
            ":operation" : "Venta"
        }
    }






    let items = [];

    function onQuery(err, data) {
      if (err) {
          console.log(err)
          return res.status(500).json({error:err})
      } else {
          console.log("Query succeeded.");
          for(let i=0;i<data.Items.length;i++)
            data.Items[i].price = string.formatNumber(`${data.Items[i].price}`);

          items = items.concat(data.Items);
          if (typeof data.LastEvaluatedKey != "undefined") {
              console.log("Querying for more...");
              params.ExclusiveStartKey = data.LastEvaluatedKey;
              dynamoDb.query(params, onQuery);
          } else {
              return res.json(items);
          }
      }
    }

    dynamoDb.query(params, onQuery);


})





/***
  Function: http get  method
  Description: Method to get all the properties in rent
  Output: list of properties
**/
app.get('/properties/rent',(req, res) => {

    let params = {
        TableName : PROPERTIES_TABLE,
        IndexName: "operation-index",
        KeyConditionExpression: "operation = :operation",
        ExpressionAttributeValues:{
            ":operation" : "Renta"
        }
    }


      let items = [];

      function onQuery(err, data) {
        if (err) {
            console.log(err);
            return res.status(500).json({error:err})
        } else {
            console.log("Query succeeded.");
            for(let i=0;i<data.Items.length;i++)
              data.Items[i].price = string.formatNumber(`${data.Items[i].price}`);

            items = items.concat(data.Items);
            if (typeof data.LastEvaluatedKey != "undefined") {
                console.log("Querying for more...");
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                dynamoDb.query(params, onQuery);
            } else {
                return res.json(items);
            }
        }
      }

      dynamoDb.query(params, onQuery);

})















/**********************************notes************************/
// sólo acepta imágenes menores a 5mb
/**************************************************************/

/***
  Function: http post method
  Description: Method to upload an image of a property
  Input: image and propertyId
  Output: verified message
**/
const uploads3 = multer({limits: { fileSize: process.env.MAX_SIZE_IMAGE }});
app.post('/properties/upload/image',uploads3.single("image"),function(req, res, next) {
    let parameters = req.body;
    let parameterAttributes = {
      propertyId: { required: true, type: 'string' }
    };
    let errorMessage =  prm.checkParameters(parameters, parameterAttributes);
    if (errorMessage) {
      console.log(errorMessage);
      return res.status(400).json(errorMessage);
    }
   if(req.file  && req.file['mimetype'].startsWith("image")) {
      let newName = parameters.propertyId+"/"+Date.now().toString();
      s3.putObject( {
        Body: req.file['buffer'],
        Bucket: PROPERTIES_IMAGES,
        Key: newName,
        ContentType: req.file['mimetype'],
        ACL: 'public-read'
      }, (err, data)=> {
         if (err){
            console.log(err, err.stack);
            return res.status(400).json({error:"the image can't be stored"})
         }else
            return res.json({url: `${process.env.URL_PROPERTIES_IMAGES}/${newName}`});

      });

  }else
    return res.status(400).json({error:'image is required'});

});

























/***
  Function: http post method
  Description: Method to erase an image of a property
  Input: name of the images, propertyId
  Output: verified message
**/
app.post('/properties/erase/image',function(req, res, next) {
  let parameters = req.body;
  let parameterAttributes = {
    propertyId: { required: true, type: 'string' },
    nameFile:{required: true, type:'string'}
  };
  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

  if (errorMessage) {
    console.log(errorMessage);
    res.status(400).json(errorMessage);
  }

  let params = {
    Bucket: PROPERTIES_IMAGES,
    Key: parameters.propertyId + "/" + parameters.nameFile
  };

  s3.headObject(params, function (err, metadata) {
    if (err && err.code === 'NotFound')
      res.status(400).json({error: "image does not exist",name: parameters.nameFile});
    else {
      s3.deleteObject(params, (err, data)=> {
        if(err){
          console.log(err);
          return res.status(400).json({ error: 'Could not erase the image' });
        }else
          return res.json({message: "image deleted",name: parameters.nameFile});
       });
    }
  });

});



module.exports.handler = serverless(app);
