// Helper function for checking a parameter type and if its required
function checkParameter(name, value, required, type) {
  if (required===true  && ( value === undefined || value===null || value.toString().length===0 ) )
    return { error: 'parameter "' + name + '" is required' };
  else if (value!==undefined && value!==null && typeof value !== type)
    return { error: 'parameter "' + name + '" must be ' + type, type: typeof value };
  else
    return;

}



const cleanParameters = (parameters) =>{
  for(value in parameters)
    if( parameters[value]===undefined || parameters[value]===null || parameters[value].toString().length===0)
      delete parameters[value]
}


module.exports.checkParameters = function (parameters, parameterAttributes) {
  let errorMessage;

  for(let key in parameterAttributes) {
    errorMessage = checkParameter(key, parameters[key],
      parameterAttributes[key]['required'], parameterAttributes[key]['type']);

    if (errorMessage) {
      console.log(errorMessage);
      return errorMessage;
    }
  }


  cleanParameters(parameters)
  return;
};





module.exports.infoApiRect = infoApiRect={
  lat:"N",
  lng:"N",
  bedrooms: "N",
  bathrooms:"N",
  parkingLots:"N",
  imagesUrls:"L",
  propertyId:"S",
  hashKey:"N",
  pageSource: "S"
};
